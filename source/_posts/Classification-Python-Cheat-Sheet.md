---
title: Classification_Python Cheat Sheet
date: 2023-01-08 12:11:52
tags:
    - LULC
    - Python
    - Cheat sheet
    - Machine Learning
    - Deep Learning
categories:
    - Python
---

Cheat sheet of Land cover land use classification using python and others.

# Introduction

## filepath and open tif file


```python
import rasterio

l8_path     = '../data/landsat_image.tif'
labels_path  = '../data/labels_image.tif'

full_dataset = rasterio.open(l8_path)
img_rows, img_cols = full_dataset.shape
img_bands = full_dataset.count
print(full_dataset.shape) # dimensions
print(full_dataset.count) # bands
```

## Open our raster dataset

```python

import rasterio

landsat_dataset = rasterio.open(l8_path)
```

## How many bands does this image have?

```python
num_bands = landsat_dataset.count
print('Number of bands in image: {n}\n'.format(n=num_bands))
```

## How many rows and columns?

```python
rows, cols = landsat_dataset.shape
print('Image size is: {r} rows x {c} columns\n'.format(r=rows, c=cols))
```

## What driver was used to open the raster?

```pyhton
driver = landsat_dataset.driver
print('Raster driver: {d}\n'.format(d=driver))
```

## What is the raster's projection?

```python
proj = landsat_dataset.crs
print('Image projection:')
print(proj)
```

## Open up the dataset and read into memory

```python
landsat_image = landsat_dataset.read()
landsat_image.shape
```

## NDVI Calculate

```python
bandNIR = landsat_image[4, :, :]
bandRed = landsat_image[3, :, :]

ndvi = np.clip((bandNIR.astype(float) - bandRed.astype(float)) / (bandNIR.astype(float) + bandRed.astype(float)), -1,1)
```

## Histogram

```python
import matplotlib.pyplot as plt

plt.hist(x = '需要处理的数据',bins = '需要分割的精度')
plt.xlabel('x轴标签')
plt.ylabel('y轴标签')
plt.title('总标题')
plt.show()
```