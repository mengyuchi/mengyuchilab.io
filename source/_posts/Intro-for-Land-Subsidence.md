---
title: Intro for Land Subsidence
date: 2022-03-21 11:42:23
tags:
    - Land Subsidence
    - Reposititory
categories:
    - SAR
---

# Introduction for Land Subsidence



> **Land subsidence**, with decreasing land elevation, has become an important environmental geological phenomenon<!-- more -->, and a common urban geological disaster in many large cities. More than 150 cities all over the world have endured land subsidence with rates up to tens of centimeters per year[1].
>
> Land subsidence is a slow, long-term geological disaster. It can be caused by natural compaction and human activities. However, most subsidence is caused by anthropogenic activities, including the exploitation of groundwater, oil, and other fluid and solid resources. In fact, the most common cause of land subsidence is the excessive extraction of groundwater[2].
>
> Current land subsidence monitoring techniques include leveling, GPS (global positioning system), and synthetic aperture radar interferometry (InSAR). Although leveling and GPS techniques provide precise measurements, they are very *time consuming*.

Land subsidence increases the risk of *environmental* and *structural hazards* such as **seawater intrusion**; **building**, **bridge**, **road**, and **pipe destruction**; and **inaccurate leveling**.

The Vietnamese Mekong delta is subsiding due to a combination of **natural and human-induced causes**[3]. Over the past several decades, large-scale anthropogenic land-use changes have taken place as a result of increased agricultural production, population growth and urbanization in the delta. Land-use changes can alter the hydrological system or increase loading of the delta surface, amplifying natural subsidence processes or creating new anthropogenic subsidence.


## InSAR and DInSAR 

Compared with these methods (**GPS**, **leveling**), the InSAR technique can obtain a wide range of surface deformation information and its monitoring precision is high, reaching the millimeter scale. In the differential InSAR technique (D-InSAR), land displacement is calculated in the radar **line of sight (LOS)** direction of a satellite (or an airplane) by computing the phase difference between two temporally separated SAR images.

GPS/leveling is a term covering the efficient determination of physical heights (orthometric, normal, or normalorthometric) based on GNSS-positioning technologies and the geoid.

**Leveling**, however, is a time consuming and expensive technology, and additionally its practical applicability requires the establishment and maintenance (periodic remeasurement) of large continental to national scale infrastructure of height reference networks[4].

### Advantage and Disadvantage 

> Compared with traditional earth observation methods, such as global positioning system (GPS), leveling, and layer-wise mark measurement, Interferometric Synthetic Aperture radar (InSAR) is a new earth observation technique used to obtain ground deformation information[1].
>
> Synthetic Aperture Radar differential interferometry (DInSAR), which was developed from the InSAR technique, has the advantages of wide coverage, working on *all-weather conditions*,  and *high-precision* monitoring of ground deformation. However, the DInSAR technique is limited by *spatial* and *temporal decorrelation*, *atmospheric errors*, *orbit errors*, and *terrain errors*. The **Persistent scatterer InSAR (PS-InSAR)** and **Small Baseline Subset InSAR (SBAS-InSAR)** techniques can overcome the limitations of DInSAR to some degree by analyzing the pixels that retain stable scattering characteristics in a time series to obtain ground deformation information.
>
> Moreover, the SBAS-InSAR technique can reduce the decorrelation influence by recognizing good coherence pixels without strong backscatter and then obtaining time series deformation.

### PS-InSAR and SBAS-InSAR 

However, this calculation contains *atmospheric errors*, *orbit errors*, and *terrain errors*. To overcome these limitations, permanent scatterer interferometry (PSI) and small baseline subset (SBAS) interferometry were used to monitor land subsidence.

**PSI** can analyze the scattering characteristics of permanent scatterer (PS) points for extracting the surface deformation information. 

**SBAS** selects interferograms with small spatial and temporal baselines that are substantially coherent to improve the accuracy of the deformation monitoring.



[1]: https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1 "Chaofan Zhou, Huili Gong. Land Subsidence Response to Different Land Use Types and Water Resource Utilization in Beijing-Tianjin-Hebei, China"
[2]: https://www.tandfonline.com/doi/full/10.1080/15481603.2016.1227297 "Chaofan Zhou, Huili Gong. Land Subsidence under different land use in the eastern Beijing Plain, China 2005-2013 revealed by InSAR timeseries analysis"
[3]: https://pubmed.ncbi.nlm.nih.gov/29649716/ "Minderhoud PSJ, Coumou L, Erban LE, Middelkoop H, Stouthamer E, Addink EA. The relation between land use and subsidence in the Vietnamese Mekong delta. Sci Total Environ. 2018 Sep 1;634:715-726. doi: 10.1016/j.scitotenv.2018.03.372"
[4]: http://www.diva-portal.org/smash/get/diva2:420783/FULLTEXT01.pdf "Ambrus Kenyeres. GPS/Leveling"