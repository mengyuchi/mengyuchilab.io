---
title: Maps_Xuzhou
date: 2023-01-11 10:38:36
tags:
    - Xuzhou
    - Maps
    - LULC
categories:
    - LULC
---

This post shows some land use maps in xuzhou collected during LULC classification research,

# Land Use Map

1. [徐州市土地利用总体规划(2006-2020)](http://zrzy.jiangsu.gov.cn/gtapp/nrglIndex.action?type=2&messageID=2c9082b56434dae1016464568fdc06e0)

![Land Use Map 2014](land_use_map_2014.jpg)


