---
title: Land Subsidence Susceptibility Mapp using MATLAB
date: 2023-03-21 17:20:46
tags:
    - Land Subsidence Susceptibility Map
    - Land Subsidence
    - ANFIS
    - MATLAB
categories:
    - Land Subsidence
---

Processes of land subsidence susceptibility map using MATLAB.

# Dataset

## Topographic based

### Distance to Stream and Steam Density

1. **In ArcGIS**[1]:

- Fill

- Flow Direction

- Flow Accumulation

- Con

2. In **SAGA GIS**[2]:

- Load `D:\DATA\LSS\DEM\tif\dem_fill_xz.tif` in SAGA

- `Terrain Analysis`— `Compound Analyses` — `Basic Terrain Analysis`

![Generate slope using SAGA](slope_SAGA.png)

- Save `slope`, `aspect`, `TWI` as tiff file. `Import/Export`—`GDAL/OGR`—`Export GeoTIFF`

- Catchment Area: `Terrain Analysis`—`Hydrology`—`Melton Ruggedness Number`


[1]: https://blog.csdn.net/qq_33657870/article/details/109580473
[2]: https://blog.csdn.net/GIS_feifei/article/details/127785374