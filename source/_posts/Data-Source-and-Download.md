---
title: Data_Source and Download
date: 2023-01-11 11:38:38
tags:
    - Dataset
    - Sentinel
    - Download
categories:
    - GIS
    - Remote Sensing
---

Datasets and it's source

# Land Cover and Land Use

1. World Cover数据集是欧空局联合全球多家科研机构，共同制作的2020年全球10米土地覆盖产品。该数据产品分辨率为10米。土地利用类型数据来源于官网(https://viewer.esa-worldcover.org/worldcover)。

![world cover](world_cover.png)
