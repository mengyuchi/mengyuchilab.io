---
title: Getting started with LaTex, VS Code and Overleaf
date: 2022-09-08 11:30:27
banner_img: /2022/09/08/Getting-started-with-LaTex-VS-Code-and-Overleaf/banner.jpg
index_img: /2022/09/08/Getting-started-with-LaTex-VS-Code-and-Overleaf/latex_vscode.jpg
tags: 
    - LaTex
    - Visual Studio Code
categories:
    - Tools
---

# Intro

Start learning to write thesis, papers, reposts, slides or everything you want using LaTex in Visual Studio Code linked with github and overleaf. 

# LaTex

> **LaTeX** is a high-quality typesetting system; it includes features designed for the production of technical and scientific documentation. LaTeX is the de facto standard for the communication and publication of scientific documents. LaTeX is available as free software[1].

# Tex Live

> **TeX Live** is intended to be a straightforward way to get up and running with the TeX document production system. It provides a comprehensive TeX system with binaries for most flavors of Unix, including GNU/Linux and macOS, and also Windows. It includes all the major TeX-related programs, macro packages, and fonts that are free software, including support for many languages around the world. Many Unix/GNU/Linux operating systems provide TeX Live via their own distributions and package managers[2]. 

# LaTex Workshop in VS Code

> LaTeX Workshop is an extension for Visual Studio Code, aiming to provide core features for LaTeX typesetting with Visual Studio Code[3].

Make sure after all installations, the LaTeX distribution in system PATH.

## Latex Errors

### Overfull \hbox (10.8622pt too wide)

```latex
        \begingroup
        \setlength{\tabcolsep}{10pt} % Default value: 6pt
        \renewcommand{\arraystretch}{1.5} % Default value: 1
        \begin{table}[h]
            \begin{tabular}{llll}
            Chairman:                & Univ. -Prof.\@ Dr.\@ Frank Othengrafen      &  &  \\
            Supervisor and Reviewer: & Univ.-Prof.\@ Dr\@. habil Nguyen Xuan Thinh &  &  \\
            Supervisor and Reviewer: & Univ.-Prof.\@ Dr.-Ing Ralf Bill             &  &  \\
            \end{tabular}
        \end{table}
        \endgroup
```

**Solution**

After typesetting a LaTeX document Overleaf will often display messages which report an underfull or overfull \hbox (or \vbox), along with so-called badness values for underfull boxes or amounts by which boxes are overfull. Firstly, it is important to note these messages are not errors. They are warnings or, more correctly, “diagnostic messages” which originate from inside the TeX engine being used to typeset your LaTeX document[5].

> You can use the [microtype package](https://ctan.org/pkg/microtype?lang=en) which takes advantage of micro-typographic extensions introduced by pdfTeX—producing higher-quality typesetting which can also result in fewer bad boxes[6].

> I assume you get this warning because your tabular is (a little bit) wider than your `\textwidth`[7]. 

Solutions include:

> enlarging the `\textwidth` a little
> narrowing down the tabular by a little
> adding `\hfuzz=0.64pt` somewhere in the preamble
> The `\hfuzz` setting specifies the maximum amount of box-overflow before a warning message will be issued.

### \printglossary not working

Acronyms are not showed after compiling in VS Code. Problem occured because a sort list should be included when useing `glossary`. At this point, we use **solution 2**.

**Solution 1**

After searching for two hours, finally found a solution online[8].

> Using `\makenoidxglossaries` and `\printnoidxglossaries` seems to apparently solve the problem.

> I thought that I had already tried them but I guess I was wrong. However, I still don't know why the previous code does not work in the given project.

Also see `Glossary User Manual`[9].

> The basic idea behind the glossaries package is that you first define your entries (terms, symbols or abbreviations). Then you can reference these within your document (like \cite or \ref). You can also, optionally, display a list of the entries you have referenced in your document (the glossary). This last part, displaying the glossary, is the part that most new users find difficult. There are three options available with the base glossaries package (Options 1–3). The glossaries-extra extension package provides two extra options for lists (Options 4 and 5) as well as an option for standalone descriptions within the document body (Option 6).

**Solution 2**

> Another possibility is to use the `automake` package option. This will try to use TeX's shell escape to run `makeindex`:

```latex
\usepackage[acronym,toc,shortcuts,automake]{glossaries}
```

**Solution 3**

> This final method doesn't do any sorting or indexing. All defined entries are listed, regardless of whether they've been used in the document. Entries are listed in order of definition.

**Summary[10]**

- Options 1 and 5 are the simplest as they don't require any external tools.
- Options 2, 3 and 4 all require an external tool which needs to be incorporated into your build process. How you do this depends on your text editor.
- Option 2 doesn't require any additional software (`makeindex` is precompiled and available with all modern TeX distributions).
- Option 3 requires `Perl`.
- Option 4 requires `Java`.
- Options 3 and 4 work best for `UTF-8` non-English sort values.





# Github

> If you are a developer, you might be used to versioning everything (hopefully). So why not use GIT in your LaTeX documents as well?

> If you are only editing it locally, it is just as simple as creating any other repository. But this starts shining when you want to work with others, or you want people to be able to check your work without you heaving to send it to them every time. On top of that, uploading your content to a remote GIT repository can protect you in case of a catastrophic failure in your personal machine[4].

## Error about contributions not display correctly in Github

The reason of contributions are not correctly dispalyed in Github is wrong git setting. After run 

```shell
git config --global user.email "youremail@yourdomain.com"
git config --global user.name "Your Name"
```

everything works just fine.





[1]: https://www.latex-project.org/ 'Latex'
[2]: https://www.tug.org/texlive/ "Tex Live"
[3]: https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop "Visual Studio Code LaTeX Workshop Extension"
[4]: https://medium.com/@rcpassos/writing-latex-documents-in-visual-studio-code-with-latex-workshop-d9af6a6b2815 "Writing LaTeX Documents In Visual Studio Code With LaTeX Workshop"
[5]: https://www.overleaf.com/learn/how-to/Understanding_underfull_and_overfull_box_warnings#Introduction "Overleaf"
[6]: https://www.overleaf.com/learn/how-to/Understanding_underfull_and_overfull_box_warnings#Common_problems_and_how_to_fix_them "Overleaf" 
[7]: https://tex.stackexchange.com/questions/6237/overfull-hbox-warning-when-using-tabular "StackExchange"
[8]: https://stackoverflow.com/questions/66924808/unable-to-show-the-glossary-with-printglossary-in-latex "Stackoverflow"
[9]: https://mirror.informatik.hs-fulda.de/tex-archive/macros/latex/contrib/glossaries/glossaries-user.html#tab:options "User Manual for glossaries.sty v4.49"
[10]: https://tex.stackexchange.com/questions/426010/printnoidxglossaries-vs-printglossaries