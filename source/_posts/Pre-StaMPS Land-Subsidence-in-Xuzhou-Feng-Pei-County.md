---
title: Pre-StamPS Land Subsidence in Xuzhou Feng-Pei County using StaMPS in LiDO3
date: 2022-04-07 20:56:30
tags:
  - InSAR 
  - Land Subsidence
  - Sentinel1-A
  - PS-InSAR
  - LiDO3
  - Xuzhou
  - StaMPS-InSAR
  - snap2stamps
categories:
  - SAR 
---

# Intro

This post is about working in Land Subsidence in Feng-Pei county in Xuzhou city. Cause the only free and available SAR dataset in this area is Sentinel-1A, so we downloaded 20 scenes SLC <!-- more -->data to process.



# Dataset

![Sentinel data overview by ASF](Dataset.png)

# Process

In this process, we use snap2stamps to prepare INSAR_master_date folder for StaMPS. 

## Anaconda

Cause snap2stamps needs python 2 environment, so we use anaconda[1] to manage python environment and easy switch between python3 and python 2.

Switching between Python 2 and Python 3 environments[2]:

```shell
$ conda create --name py2 python=2.7
$ conda activate py2 # active python 2.7
$ conda deactivate # deactivate
$ pyhon 
Python 2.7.18 |Anaconda, Inc.| (default, Apr 23 2020, 17:26:54) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()

(base) C:/Users/mengy>python 
Python 3.9.7 (default, Sep 16 2021, 16:59:28) [MSC v.1916 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.

```

## SNAP prepare

- File location: File	I:/SAR/Data/Xuzhou/Sentinel/Path_142/Frame_109

- Master select[3] - *Radar / Interferometric / InSAR Stack* Overview: 28OCT2016 **2016-10-28**

- Perform subsetting - *Radar / Sentinel-1 TOPS / S-1 TOPS Split*

> - subswath (IW 2)
> - polarization (vertical sent, vertical returned - VV), and
> - bursts (using the bursts-slider) in the respective tab (1-7)
> - Graph Builder (I:/SAR/Data/Feng_Pei/Auxiliary)
> - Batch Processing (I:/SAR/Data/Feng_Pei/Pre)

- Subset 

> POLYGON ((117.335 33.977, 116.61 33.977, 116.61 35.122, 117.355 35.122, 117.335 33.977))

> POLYGON ((116.61 34.130, 117.335 34.130, 117.335 35.117, 116.61 35.117, 116.61 34.130))


## snap2stamps

Note that snap2stamps requires Python 2.7, so we work the fellowing step in ubuntu, wsl. 

### Python environmeng

- Download python2 

```shell
$ sudo apt install python
$ python --version
$ sudo apt install pip
```

- Change the Python3 default version in Ubuntu[4]

```shell
$ sudo update-alternatives --config python
$ update-alternatives: error: no alternatives for python3 
$ sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 40
$ sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 20
$ sudo update-alternatives --config python
```

- Start working in python2 environment

```
$ python2
Python 2.7.18 (default, Mar  8 2021, 13:02:45)
[GCC 9.3.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

### Config snap2stamps 

```shell
######### CONFIGURATION FILE ######
###################################
# PROJECT DEFINITION
PROJECTFOLDER=/mnt/i/SAR/Data/Feng_Pei/project
GRAPHSFOLDER=/mnt/i/SAR/Data/Feng_Pei/project/graphs
##################################
# PROCESSING PARAMETERS
IW1=IW2
MASTER=/mnt/i/SAR/Data/Feng_Pei/project/master/S1A_IW_SLC__1SDV_20161028T101204_20161028T101231_013689_015F45_AEF2_split.dim
##################################
# AOI BBOX DEFINITION
LONMIN=116.61
LATMIN=34.130
LONMAX=117.335
LATMAX=35.117
##################################
# SNAP GPT 
GPTBIN_PATH=/mnt/c/'Program Files'/snap/bin/gpt.exe
#GPTBIN_PATH=/home/yuchi/snap/bin/gpt
##################################
# COMPUTING RESOURCES TO EMPLOY
CPU=4
CACHE=16G
##################################

```
### Working in snap2stamps

- Go to snap2stamps folder and start processing.

```shell
yuchi@DESKTOP-C79LC47:~$ cd /mnt/i/Software/snap2stampsbin
yuchi@DESKTOP-C79LC47:/mnt/i/Software/snap2stamps$
```

#### *Error 1 SyntaxError: invalid syntax*:
> ```
> >>> python slaves_prep.py project.conf
>  File "<stdin>", line 1
>    python slaves_prep.py project.conf
>                     ^
> SyntaxError: invalid syntax
> ```

**Solution**[5]: 
>  Looks like your problem is that you are trying to run python test.py from within the Python interpreter, which is why you're seeing that traceback.
> 
> Make sure you're out of the interpreter, then run the python test.py command from bash or command prompt or whatever.

```shell
$ sudo update-alternatives --config python
$ pip install pathlib
$ python slaves_prep.py project.conf
```
#### *Error2 Can't install pathlib and pip2 in python2*

**Solution**[6]:
>
> ```shell
> $ wget https://bootstrap.pypa.io/get-pip.py
> $ sudo python2 get-pip.py
> $ pip 20.3.4 from /home/yuchi/.local/lib/python2.7/site-packages/pip (python 2.7)
> $ pip2 install pathlib
> $  python2
> Python 2.7.18 (default, Mar  8 2021, 13:02:45)
> [GCC 9.3.0] on linux2
> Type "help", "copyright", "credits" or "license" for more information.
> >>> help('modules')
> ```

- Start process snap2stamps

```shell

python slaves_prep.py project.conf

python splitting_slaves.py project.conf

python coreg_ifg_topsar.py project.conf

python stamps_export.py project.conf

```

#### *Error 3 subprocess.popen no such file or directory in* 
```pyhton
 python splitting_slaves.py project.conf 
 ```

**Solution**: Try add an extra parameter shell=True to the Popen call[7].

#### *Error 4*: SNAP STDOUT:/mnt/i/SAR/Data/Feng_Pei/project/graphs/splitgraph2run.xml: 1: /mnt/c/Program: not found in

```
python splitting_slaves.py project.conf
```

**Solution**: Edit conf file GPTBIN_PATH=/mnt/c/'Program Files'/snap/bin/gpt.exe instead of GPTBIN_PATH=/mnt/c/Program Files/snap/bin/gpt.exe

#### *Error 5 GDAL not found on system in step2*
```shell
[18] Folder: 20170225
/mnt/i/SAR/Data/Feng_Pei/project/slaves/20170225
['/mnt/i/SAR/Data/Feng_Pei/project/slaves/20170225/S1A_IW_SLC__1SDV_20170225T101159_20170225T101226_015439_01957D_63E5.zip']
FILE(s) : /mnt/i/SAR/Data/Feng_Pei/project/slaves/20170225/S1A_IW_SLC__1SDV_20170225T101159_20170225T101226_015439_01957D_63E5.zip
["/mnt/c/'Program Files'/snap/bin/gpt.exe", '/mnt/i/SAR/Data/Feng_Pei/project/graphs/splitgraph2run.xml', '-c', '16G', '-q', '4']
SNAP STDOUT:INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: GDAL not found on system. Internal GDAL 3.0.0 from distribution will be used. (f1)
INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: Internal GDAL 3.0.0 set to be used by SNAP.
INFO: org.esa.snap.core.gpf.operators.tooladapter.ToolAdapterIO: Initializing external tool adapters
INFO: org.esa.snap.core.util.EngineVersionCheckActivator: Please check regularly for new updates for the best SNAP experience.
INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: Internal GDAL 3.0.0 set to be used by SNAP.
Usage:
  gpt <op>|<graph-file> [options] [<source-file-1> <source-file-2> ...]
  Description:
  This tool is used to execute SNAP raster data operators in batch-mode. The
  operators can be used stand-alone or combined as a directed acyclic graph
  (DAG). Processing graphs are represented using XML. More info about
  processing graphs, the operator API, and the graph XML format can be found
  in the SNAP documentation.
```

Regarding GDAL versions compatible with SNAP, these are:

Windows: 2.1.x - 3.0.x
Linux: 2.0.x - 3.0.x
Mac: 2.0.x - 3.0.x

or by manually updating the configuration (for users using SNAP with no GUI):
in .snap/etc/s2tbx.properties, add: s2tbx.dataio.gdal.installed=false[19]

And the file is hidden in home/yuchi/.snap/etc/s2tbx.properties, use *show hidden* to locate file.

**Solution**

Change of two attributes in range and azimute for snap V.7 and later[8].

```xml
Try to change 
<useSuppliedShifts>false</useSuppliedShifts> to 
<useSuppliedRangeShift>false</useSuppliedRangeShift>
<useSuppliedAzimuthShift>false</useSuppliedAzimuthShift> 
in coreg_ifg_computation.xml file This option is changed to two attributes in range and azimute for snap V.7
```

**IMPORTANT: snap2stamps is a dead end, change to SNAP desktop!**

**UPDATE**

This maybe caused by configuration between windows and Ubuntu wsl, so we try to install SNAP in Ubuntu directly.

- How to install SNAP in Ubuntu command line?

  > Building S1TBX from the source[12]

  ```shell
  $ git clone https://github.com/senbox-org/s1tbx.git
  $ git clone https://github.com/senbox-org/snap-desktop.git
  $ git clone https://github.com/senbox-org/snap-engine.git
  $ git clone https://github.com/senbox-org/snap-installer.git
  $ mvn clean install
  #If unit tests are failing, you can use the following to skip the tests
  $ mvn clean install -Dmaven.test.skip=true
  ```

#### *Error 6 mvn clean install*

Building SNAP from sources : errors during the “mvn clean install”[13].

```
[INFO] Ceres Runtime Library 9.0.0-SNAPSHOT ............... FAILURE
```

**Solution by marpet @ STEPForum**

This happens when you use Java 9 or 10. SNAP is not yet compatible with these Java versions[13].
You can either switch to Java 8, that’s what I would suggest, or you can change how Java is handling these incompatibility issues.

To switch between installed java versions, use the ```update-java-alternatives``` command[14].

Installing Open JDK 8 on Debian or Ubuntu Systems[15].


#### SNAP successly installed in Ubuntu

Check post '[Setup and Problem-Solving in WSL2](https://mengyuchi.github.io/2022/04/14/Setup-and-Problem-Solving-in-WSL2/)' for details in SNAP install in WSL. 

### Add breakpoints in python 

The module pdb defines an interactive source code debugger for Python programs[10].
```shell
yuchi@DESKTOP-C79LC47:/mnt/i/Software/snap2stamps/bin$ python -m pdb splitting_slaves.py project.conf
> /mnt/i/Software/snap2stamps/bin/splitting_slaves.py(15)<module>()
-> import os
(Pdb) b splitting_slaves.py:78
Breakpoint 1 at /mnt/i/Software/snap2stamps/bin/splitting_slaves.py:78
(Pdb) c
/mnt/i/SAR/Data/Feng_Pei/project
/mnt/i/Software/snap2stamps/graphs
IW2
/mnt/c/'Program Files'/snap/bin/gpt.exe

#####################################################################

## TOPSAR Splitting and Apply Orbit


#####################################################################

> /mnt/i/Software/snap2stamps/bin/splitting_slaves.py(78)<module>()
-> k=k+1
(Pdb) p filename
*** NameError: NameError("name 'filename' is not defined",)
(Pdb) p k
0
(Pdb) p slavefolder
'/mnt/i/SAR/Data/Feng_Pei/project/slaves'
(Pdb) p acdatefolder
'20150730'
(Pdb) p args
*** NameError: NameError("name 'args' is not defined",)
(Pdb) p graphfolder
'/mnt/i/SAR/Data/Feng_Pei/project/graphs'
(Pdb)
```

#### *Error 7 Unknown element 'useSuppliedShifts'*

Error: [NodeId: Enhanced-Spectral-Diversity] Operator 'SpectralDiversityOp': Unknown element 'useSuppliedShifts'


**Solution**

```
Try to change <useSuppliedShifts>false</useSuppliedShifts> to <useSuppliedRangeShift>false</useSuppliedRangeShift> <useSuppliedAzimuthShift>false</useSuppliedAzimuthShift> in coreg_ifg_computation.xml file This option is changed to two attributes in range and azimute for snap V.7[16]
```

#### *Error 8 Java Heap Space Error*

**Solution**

```
I upgraded my system RAM from 8 Gb to 16 Gb.
I even tried increasing the size of xmx parameter to 32Gb in SNAP interface[17].

Increasing the available memory only has an effect if your PC has this amount of RAM. It only defines how much of your RAM can be used by Java (SNAP in this case).
For example, if you have 8 GB of RAM, increasing the max value to 16 GB has no effect.
m in this context stands or megabytes and the values should ideally be a multiple of 512[18].
```

#### *Error 9 Process stopped at 20%*

/mnt/i/SAR/Data/Feng_Pei/project//split/20150730/20150730_IW2.dim
Processing slave file :20150730_IW2.dim

SNAP STDOUT:INFO: org.esa.snap.core.gpf.operators.tooladapter.ToolAdapterIO: Initializing external tool adapters
INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: Incompatible GDAL 3.3.2 found on system. Internal GDAL 3.0.0 from distribution will be used.
INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: Internal GDAL 3.0.0 set to be used by SNAP.
INFO: org.esa.snap.core.util.EngineVersionCheckActivator: Please check regularly for new updates for the best SNAP experience.
INFO: org.esa.s2tbx.dataio.gdal.GDALVersion: Internal GDAL 3.0.0 set to be used by SNAP.
Executing processing graph
INFO: org.hsqldb.persist.Logger: dataFileCache open start
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Shifts written to file: /home/yuchi/.snap/var/log/IW2_range_shifts.json
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 1/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 2/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 3/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 4/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 5/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Estimating azimuth offset for blocks in overlap: 6/6
INFO: org.esa.s1tbx.sentinel1.gpf.SpectralDiversityOp: Shifts written to file: /home/yuchi/.snap/var/log/IW2_azimuth_shifts.json
....10%....20%.
Finished process in 761.911759138 seconds.

**Solution**

### Working in SNAP Desktop

Work base on tutorial written by Amir Shafaei[9].

- Applying TOPSAR split and precise orbit on all the SLC data

- Finding the master SLC: 28.10.2016

- Making a Stack by back geocoding from the processed data while the master SLC is set at top of the table. 
  Radar>Coregistration >s1top Coregistration> s1 back geocoding

- TopSAR deburst
  Radar > sentinel 1 top > S1 top deburst






[1]: https://www.anaconda.com/ "Anaconda official"
[2]: https://docs.anaconda.com/anaconda/user-guide/tasks/switch-environment/ "Anaconda switch between python2 and python3"
[3]: https://gitlab.com/Rexthor/gis-blog/-/blob/master/StaMPS/2-3_snap2stamps.md "Tutorial of snap2stamps"
[4]: https://linuxhint.com/update_alternatives_ubuntu/ "How to Use update-alternatives Command on Ubuntu"
[5]: https://stackoverflow.com/questions/13961140/syntax-error-when-using-command-line-in-python "syntax error when using command line in python"
[6]: https://www.gungorbudak.com/blog/2018/08/02/correct-installation-and-configuration-of-pip2-and-pip3/ "Correct Installation and Configuration of pip2 and pip3"
[7]: https://stackoverflow.com/questions/9935151/popen-error-errno-2-no-such-file-or-directory "Popen error: [Errno 2] No such file or directory"
[8]: https://forum.step.esa.int/t/snap2stamps-error/16768/150?page=8 "STEP Forum"
[9]: https://forum.step.esa.int/t/psi-using-snap-stamps-workflow-in-windows-subsystem-for-linux-wsl/23004 "PSI using SNAP-STaMPS workflow in Windows Subsystem for Linux (WSL)"
[10]: https://docs.python.org/3/library/pdb.html "pdb — The Python Debugger"
[11]: https://realpython.com/python-debugging-pdb/ "Python Debugging With Pdb"
[12]: https://github.com/senbox-org/s1tbx "The SENTINEL-1 Toolbox"
[13]: https://forum.step.esa.int/t/building-snap-from-sources-errors-during-the-mvn-clean-install/10626 "Building SNAP from sources : errors during the mvn clean install"
[14]: https://askubuntu.com/questions/740757/switch-between-multiple-java-versions "Switch between multiple java versions"
[15]: https://docs.datastax.com/en/jdk-install/doc/jdk-install/installOpenJdkDeb.html "Installing Open JDK 8 on Debian or Ubuntu Systems"
[16]: https://forum.step.esa.int/t/snap2stamps-error/16768/150?page=8 "STEP Forum"
[17]: https://forum.step.esa.int/t/java-heap-space-error/4062/18 "STEP Forum"
[18]: https://forum.step.esa.int/t/s1-tops-coregistration-error-java-heap-space-error/6426/15 "STEP Forum"
[19]: https://forum.step.esa.int/t/snap-8-0-gpt-error-gdal-version-netcdf-c-library-error/26259/2 "STEP Forum"