---
title: Research about influences of Xuzhou metro construction upon land sustainability
date: 2022-05-18 21:13:44
banner_img: /2022/05/18/Research-about-influences-of-Xuzhou-metro-construction-upon-land-sustainability/xuzhou_railway.jpg
index_img: /2022/05/18/Research-about-influences-of-Xuzhou-metro-construction-upon-land-sustainability/line1.jpg
tags: 
  - Xuzhou
  - Land Subsidence
categories:
  - Xuzhou
---

# Intro

This post is about the influences of metro contrution upon land subsidence in Xuzhou. 

# About Xuzhou metro

The Xuzhou Metro is a rapid transit system in Xuzhou, Jiangsu Province, China. Construction began in February 2014, and Line 1 was opened on September-28-2019, Line 2 was opened on November-28-2020, and Line 3 was opened on June-28-2021[1].

By 2020, the network is scheduled to comprise three lines with a total length of about 67 kilometres. The total investment is expected to be 44.3 billion renminbi.The system is planned to comprise 11 lines and 323 kilometres of track when fully built out.

In its first year of operation the ridership was 7.5 million (2019).

# Line 1

Line 1 began construction in February 2014. The first phase is 21.967 kilometres (13.6 mi) in length, and has 18 stations (17 underground and one elevated). It runs from Luwo in the west to Xuzhoudong Railway Station in the east. In June 2018, the civil engineering construction work was reported to be 95% complete, leaving mostly track laying, decorations, and electromechanical installations to be completed. On August 30, 2018, railway work was declared complete, and electromechanical installations and decorations declared as "under intense progress". It was opened on September 28, 2019. Line 1 runs in east–west, starting from Luwo Station in Tongshan District in the west, and ending at Xuzhoudong Railway Station in Jiawang District in the east. It passes through Quanshan District, Gulou District, and Yunlong District, covering the east–west passenger flow corridor of Xuzhou City. Important nodes in Xuzhou city downtown, Bashan Area, and Chengdong New District connect to commercial centers such as People's Square, Pengcheng Square, Huaihai Square and Wanda Plaza, and quickly connect with the two major comprehensive passenger transport hubs, Xuzhou Railway Station and Xuzhoudong Railway Station.

Line 1 opened on September 28, 2019. This was the first metro line in Xuzhou city. As of November 2020, Line 1 has a total length of 21.97 km (13.65 mi), of which the line is elevated for 0.571 km (0.355 mi), underground for 20.996 km (13.046 mi), and at-grade for 0.4 km (0.25 mi). There are a total of 18 stations, including one elevated station and 17 underground stations. Rolling stock for line 1 is six-section B-type trains. As of November 2020, the average daily ridership of Line 1 is 56,500, and the single-day ridership record is 150,000, set on October 2, 2019. Yearly ridership for 2020 on line 1 was 19 million.

# Line 2

Line 2, opened on November 28, 2020, is 24.25 kilometres (15.07 mi) long with 20 underground stations. The line is fully underground Xuzhou Metro Line 2 was the second metro line in Xuzhou city to open. It is a north–south to east–west backbone line that runs through the old city and Xuzhou New District, covering the north and southeast passenger flow corridors of the city, connecting the Jiulishan area, the old city and the new city.

## Dalonghu - Phase 1

Station Dalonghu was successfully topped out in 2017-03-14[2], along with City-Council Station within Line 2. The duration of the contruction of Dalonghu Station was less than 1 year. 





[1]: https://en.wikipedia.org/wiki/Xuzhou_Metro 'Xuzhou Metro'
[2]: https://www.xzdtjt.com/article/11/190/2722.html "Dalonghu Station"