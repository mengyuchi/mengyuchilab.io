---
title: Land subsidence in Xuzhou using Sentinel1A PS-InSAR in SARScape
date: 2022-04-07 10:54:23
tags:
  - InSAR 
  - Land Subsidence
  - Sentinel1-A
  - PS-InSAR
  - SARScape
categories:
  - SAR 

---

# Intro

Working step and parameter setting in SARscape.

<!-- more -->

## Geocoding

lat/lon 34.15'5.27"N,117.11'10.17"E

117.18564917852
34.245620476831

G_SI_velocity_geo in SBAS result
  File: 3250.7017,3217.9978
  Data: [-0.020012]

Stuck at Geocoding (mean), waiting for response! Pls!!

**Update: After change to NIVDA, process works again!** Congrats! Keep Going! 

