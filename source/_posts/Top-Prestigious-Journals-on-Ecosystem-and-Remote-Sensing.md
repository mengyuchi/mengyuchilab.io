---
title: Top Prestigious Journals on Ecosystem and Remote Sensing
date: 2023-03-16 09:36:27
tags:
    - SCI
    - Prestigious Journals
    - Top Journals
categories:
    - Scientific  Reasearch
---

# Remote Sensing



# Ecosystem and Environmental 

## 1. Sustainable cities and society

- ISSN: 2210-6707

- 大类及分区：工程技术1区Top

- 小类及分区：
    + Green and Sustainable Science and Technology: Top 1

    + Energy and Fuels: Top 2

    + Contruction and Building Technology: Top 1


## 2. International Journal of Mining Science and Technology

- ISSN: 2095-2686

- 大区及分类: 工程技术1区Top

- 小区及分类：
    + Mining and Mineral Processing: Top 1

## 3. Science of the Total Environment

- ISSN: 0048-9697

- 大区及分类：环境科学与生态学1区Top

- 小区及分类：
    + Environmental Sciences: Top 1

## 4. Landscape and Urban Planning

- ISSN: 0169-2046

- 大区及分类：环境科学与生态学1区Top

- 小区及分类：
    + Urban Studies: Top 1

    + Environmetal Studies: Top 1

    + Ecology: Top 1

    + Regional and Urban Planning: Top 1

    + Geography, Physical: Top 1

    + Geography: Top 1

## 5. Ecological Indicators

- ISSN: 1470-160X

- 大区及分类：环境科学与生态学2区Top

- 小区及分类：
    + Environmental Sciences: Top 2

## 6. Journal of Cleaner Production

- ISSN: 0959-6526

- 大区及分类: 环境科学与生态学1区Top

- 小区及分类: 
    + Environmental Sciences: Top 1

    + Engineering, Environmental: Top 2

    + Green & Sustainable Science & Technology: Top 1





# GIS
