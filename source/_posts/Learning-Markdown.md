---
title: Getting Started with Markdown
date: 2022-03-17 13:23:18
tags:  
    - Testing
    - Reposititory
categories: 
    - Testing
---

# Getting Started with Markdown 
## A
### B
#### C

**This text is bold** 

*italicized*

<!-- more -->

> Anders Antonsen

I really like using Markdown.

I think i will use it to format all of my documents from now on.

First line with two spaces after.  
And the next line.

First line with the HTML tag after.<br>
And the next line.

I jsut love **bold text**.
I just love __bold text__.
love**is**bold

Itilicized text is the *cat's meow*.
Itilicized text is the _cat's meow_.
A*cat*meow

This text is ***really important***.
This text is ___really important___.
This is really***very***important text.

> Dorothy followed her through many of the beautiful rooms in her castle.

>A
>B
>C
>
>D

>Dorothy followed her through many of the beautiful rooms in her castle.
>
>>The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.
>>
>>>And test.

> # Looks Good!
> 
> - Revenue was off the chart.
> - Profits was higher then ever.
>
> *everything* is going according to **Plan**.

1. First item
2. Secong item
3. Third item
    1. Indented item
    2. Indented item
4. Forth item

- First item
- Secong item
    - Intended item
    - Intended item
- Third item
- Forth item

* First item
* Second item

+ First item
+ Second item

- 1909\. A great year.

- Ihink 2008 was the secong best.

- This is the first list item.
- Here's the secong list item.

    I need to add another paragraph below the second item list.
    > A blockquote would look great below the second list item.

- And here's the third list item.

At the command prompt, type `nano`.

`code`

``use `code` in your Markdown file.``

***

___

---

Search [Google](www.google.com "not Baidu.com")

<https://mengyuchi.github.io>

<mengyuchile@gmail.com>

I love supporting the **[EFF](https://eff.org)**.

This is the *[Markdown Guide](https://www.markdownguide.org)*.

See the section on [`A`](#a).

This post is based on [Markdown Guide][1]

![Me](/../../themes/next/source/images/avatar.JPG "me in Greece") 

Result see post **Markdown Image Testing**

\* without the backslash, this would be a bullet in an unordered list.

# Extended Syntax

## Tables

| Syntax | Descrption |
| -  | -  |
| Header | Title |
| Paragraph | Text |

Or use [Markdown Tables Generator][2]

see [Guide](https://www.markdownguide.org/extended-syntax/)


```javascript
{
    alert("Hello World!");
    var myVar = "Hello World!";
}
```

#Extended Syntax 

> Some of the extended syntax not working in VScode!

## Footnotes Not Working 

> Add hexo plugin 

```
    npm install hexo-footnotes --save
```

This extension is outdated, so got uninstalled.

After research we understand there are different Markdown rendering tools. In case we needed it after, here are some references that give you an idea.

[hexo-renderer-markdown-it][3]
[A blog based on Hexo about hexo-renderer-makedown-it and others][4]
[Compare of differernt renderer][5]



==Fixed by adding ***Extensions: Markdown Preview Enhanced***==


Here's a simple footnote.[^1] 

[^1]: This is the first footnote.

## Task List

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media 

[x] Write the press release
[ ] Update the website
[ ] Contact the media

Gone camping! :tent: Be back soon.
😀

:blush:

That is so funny! :joy:

I need to highlight these ==very important words==.

==very important words==

==spaces are important in highlighting texts!==

## Katex Test

Aha! $E = mc^{2}$.

$$\begin{equation} \label{eq1}
e=mc^2
\end{equation}$$

$$
\left(\beta m c^2 + c \left(\sum_{n=1}^3\alpha_n p_n\right)\right) \psi(x,t)
= i\hbar \frac{\partial \psi(x,t)}{\partial t}
$$


[1]: https://www.markdownguide.org/basic-syntax#lists-1 'Markdown'
[2]: https://www.tablesgenerator.com/markdown_tables# 'Markdown Tables Generator'
[3]: https://github.com/hexojs/hexo-renderer-markdown-it 'renderer'
[4]: https://lamirs.vercel.app/
[5]: https://bugwz.com/2019/09/17/hexo-markdown-renderer/ 'Compare renderer'

# Enjoy Markdown! 

Happy Life!