---
title: Basic concepts of Machine Learning and Deep Learning
date: 2023-01-25 10:33:38
tags:
    - Concept
    - Machine Learning
    - Deep Learning
    - Confusion Matrix
    - Accuracy
categories:
    - Machine Learning 
    - Deep Learning
---

Some basic concepts of machine learning (ML) and deep learning (DL).

# What is ML/DL

# Evaluation

Precision, Recall and Accuracy are three metrics that are used to measure the performance of a machine learning algorithm[2].

## Confusion Matrix (混淆矩阵)

![Confusion Matrix](confusion_matrix.jpg)

真正例和真反例是被正确预测的数据，假正例和假反例是被错误预测的数据。接下来的内容基本都是围绕这个四个值展开，所以需要理解这四个值的具体含义[1]：

+ TP（True Positive）：被正确预测的正例。即该数据的真实值为正例，预测值也为正例的情况；
+ TN（True Negative）：被正确预测的反例。即该数据的真实值为反例，预测值也为反例的情况;
+ FP（False Positive）：被错误预测的正例。即该数据的真实值为反例，但被错误预测成了正例的情况；
+ FN（False Negative）：被错误预测的反例。即该数据的真实值为正例，但被错误预测成了反例的情况。

## stratified sampling (分层抽样)

分层抽样法也叫类型抽样法。它是从一个可以分成不同子总体（或称为层）的总体中，按规定的比例从不同层中随机抽取样品（个体）的方法。这种方法的优点是，样本的代表性比较好，抽样误差比较小。缺点是抽样手续较简单随机抽样还要繁杂些。定量调查中的分层抽样是一种卓越的概率抽样方式，在调查中经常被使用[3]。

![stratified sampling](stratified_sampling.png)





[1]: https://zhuanlan.zhihu.com/p/405658103
[2]: https://www.nomidl.com/machine-learning/what-is-precision-recall-accuracy-and-f1-score/
[3]: https://blog.csdn.net/weixin_43981621/article/details/104396134