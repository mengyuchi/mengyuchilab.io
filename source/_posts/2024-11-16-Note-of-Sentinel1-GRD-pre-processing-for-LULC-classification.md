---
title: Note of Sentinel1 GRD pre-processing for LULC classification
date: 2024-11-16 16:10:57
tags: 
    - SNAP
    - denoising
    - SLC
    - LULC
    - Sentinel1 GRD
categories:
    - SNAP
---

This post is contains notes during process of SNAP sentinel GRD data.

1. Import GRD data

2. Subset

    - Top: 34.60
    - Bottom: 34.00
    - Left: 116.50
    - Right: 117.95

![subset](subset.png)