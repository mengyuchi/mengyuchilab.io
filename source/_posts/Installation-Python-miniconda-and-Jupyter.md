---
title: 'Installation: Python, miniconda and Jupyter'
date: 2023-01-04 12:15:28
tags:
    - Insatllation
    - Python
    - Miniconda
    - Jupyter
categories:
    - Python
---

# Python

# Miniconda installation and confirguration

# Jupyter installation and configuration

由于多个环境中只需要安装一个 Jupyter ，这里将 Jupyter 单独安装在一个环境中，对其他环境进行隔离。若安装在其他环境中，则运行 Jupyter 需要进入安装时的环境[1]。

```shell
conda create -n jupyter # 创建jupyter环境
activate jupyter # 进入该环境
conda install jupyter notebook # 安装Jupyter包
```

运行 Jupyter Notebook 前，为了在不同 Conda 环境下运行文件，需要在`jupyter`环境中安装`nb_conda_kernels`包，并在其他需要用到的环境中安装`ipykernel`包.

```shell
conda activate jupyter
conda install nb_conda_kernels
conda activate [environmentName]
conda install ipykernel
```

运行 `Jupyter Notebook` 仍在`Anaconda Prompt`的 `jupyter` 环境下运行。 (之后关于 `Jupyter` 的操作均在`jupyter`环境下进行)

## Jupyter in VS Code

Create new file `*.ipynb` in VS Code.

## Familiarizing Yourself with Jupyter Notebooks

Here, we will review some essential keyboard shortcuts and notebook features, which will enable you to use notebooks competently. A notebook has two modes of operation[2]:

### Command Mode (Press `esc` to activate)

When in command mode, you can use keyboard shortcuts to create/delete/cut/paste notebook cells, and to change a cell’s type between code and markdown modes. Your selected cell will be surrounded by a blue border when you are in command mode. For a complete listing of keyboard shortcuts, toward the top of the notebook click `Help > Keyboard Shortcuts`. The most critical shortcuts are:

- create a new cell above the current cell: `a`

- create a new cell below the current cell: `b`

- delete the current cell: `dd`

- restart the notebook kernel (kill all executions and erase all defined variables): `00`

- change the current cell’s type to “Code”: `y`

- change the current cell’s type to “Markdown”: `m`

### Edit Mode (Press `Enter` to activate)

Edit mode simply permits you to type text into the selected cell. When in edit mode, the current cell will be surrounded by a green border. There are two commands for executing a cell:

- execute current cell: `<CTRL>+<ENTER>`

- execute current cell and then create a new cell below: `<SHIFT>+<ENTER>`

By default, a cell will be a code-type cell, meaning that its content will be formatted and executed as Python code. Just as you saw when using the IPython notebook, `<TAB>` can be used to perform autocomplete. Additionally, when your cursor is on the name of a Python function in your code, `<SHIFT>+<TAB>` will bring up a small window with the function’s documentations string. This is very useful.

# packages

## matplotlib

[matplotlib](http://matplotlib.org/) is a very powerful plotting library for making amazing visualizations for publications, personal use, or even web and desktop applications. `matplotlib` can create almost any two dimensional visualization you can think of, including histograms, scatter plots, bivariate plots, and image displays. For some inspiration, check out the `matplotlib` [example gallery](http://matplotlib.org/gallery.html) which includes the source code required to generate each example[5].


## matplotlib API - state-machine versus object-oriented
One part of `matplotlib` that may be initially confusing is that `matplotlib` contains two main methods of making plots - the object-oriented method, and the state-machine method. 
    
A very good overview of the difference between the two usages is provided by [Jake Vanderplas](http://jakevdp.github.io/mpl_tutorial/index.html). Specifically,

+ [Pyplot state-machine](http://jakevdp.github.io/mpl_tutorial/tutorial_pages/tut1.html)
+ [Object-oriented](http://jakevdp.github.io/mpl_tutorial/tutorial_pages/tut2.html)

If you need a primer on matplotlib beyond what is here I suggest: [Python Like you Mean It](https://www.pythonlikeyoumeanit.com/Module5_OddsAndEnds/Matplotlib.html) or the [matplotlib users guide](https://matplotlib.org/3.0.2/index.html).

In general, I think you should use the object-oriented API. While more complicated, is a much more powerful way of creating plots and should be used when developing more complicated visualizations. I always recommend the OO API.


# Errors 

## import rasterio and gdal failed


```
ImportError: DLL load failed while importing _base: The specified module could not be found.
```

**Soloution:**
> rasterio与GDAL版本不匹配。rasterio目前支持的GDAL版本为1.11.x 到 2.4.x.，而目前conda自动安装的gdal版本为3.x，因此在rasterio时需要指定GDAL版本[3].

> I encountered the same problem as you and has finished solving it. First, You need to ensure that gdal version is still 2.x. If no, just remove it[4].

Run

```shell
$ conda remove rasterio gdal -y

$ conda install -c "conda-forge/label/rasterio_dev" rasterio

$ python

(base) PS C:\Users\mengy> python
Python 3.9.12 (main, Apr  4 2022, 05:22:27) [MSC v.1916 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import rasterio
>>> print("rasterio's version is: " + rasterio.__version__)
rasterio's version is: 1.2.10
```


[1]: https://zhuanlan.zhihu.com/p/449750184
[2]: https://www.pythonlikeyoumeanit.com/Module1_GettingStartedWithPython/Jupyter_Notebooks.html#Familiarizing-Yourself-with-Jupyter-Notebooks
[3]: https://blog.csdn.net/qq_41032721/article/details/125812016
[4]: https://stackoverflow.com/questions/55596662/rasterio-and-gdal-dll-load-fail-in-pycharm
[5]: https://github.com/patrickcgray/open-geo-tutorial/blob/master/notebooks/chapter_3_visualization.ipynb 