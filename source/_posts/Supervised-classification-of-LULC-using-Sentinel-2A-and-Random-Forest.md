---
title: Supervised classification of LULC using Sentinel-2A and Random Forest
date: 2023-01-02 10:40:24
tags:
    - LULC
    - Sentinel-2
    - Random Forest
    - SNAP
categories:
    - LULC
---

This post is about some notes and steps while doing research: LULC supervised classification using Sentinal-2A dataset,and Random Forest machine learning method.

# Pre-process

Before starting process of supervised classification, we need to prepare datesets in SNAP. The dataset we use includs: 

- B2,B3,B4,B8,SRB11,SRB12,SRB5,SRB6,SRB7,SRB8A,SRB9

- BI2 (The second Brightness Index)

- NDVI

- REP (Red-Edge Position Index)

- MNDWI

After `band select` we got file `*`


# Random Forest

Random forest classifier is an ensemble tree-based learning algorithm. The random forest classifier is a set of decision trees from a randomly selected subset of the training set. It aggregates the votes from different decision trees to decide the final class of the test object[2].

# Process

## Classification class

5 classes

- Forest

- Farmland

- Water

- Construction

- Bareland

## Create training samples and validation samples

1. Create Vector Data Container



[1]: https://blog.csdn.net/lidahuilidahui/article/details/103831851
[2]: https://builtin.com/data-science/random-forest-python-deep-dive



