---
title: Markdown Image Testing
date: 2022-03-17 20:41:57
tags: 
    - Testing
categories:
    - Testing
---

# Markdown Image Testing 

<!-- more -->

## Embedding an image using markdown

> hexo-renderer-marked 3.1.0 introduced a new option that allows you to embed an image in markdown without using asset_img tag plugin.

```
post_asset_folder: true
marked:
  prependRoot: true
  postAsset: true
```

> Once enabled, an asset image will be automatically resolved to its corresponding post’s path. 

![Me](avatar.JPG "Me in Greece")

<!-- more -->

## Path to image

To understand the true ralative path to images in hexo posts using marksdown syntax, we tries several ways and the result is:

> Evenso we enabled everything mentioned before, the true path of 'avatar.JPG'is not **/Blog/source/_posts\Markdown-Image-Testing/avatar.JPG** but **/Blog/public/2022/03/17/Markdown-Image-Testing/avatar.JPG** 

# Tag Plugin



