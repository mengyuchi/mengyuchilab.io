---
title: SNAP processing SAR images for denoising
date: 2024-07-03 09:58:29
tags: 
    - SNAP
    - denoising
    - SLC
categories:
    - SNAP
---

This post is about workflow of SAR data process in SNAP before denoising.

# SNAP

## Processing of SAR data

1. Load SAR data in SNAP

2. Apply orbit file: `Radar -> Apply Orbit Files`

    This step makes no impact to origin SAR files, but modify the metadata.xml file.

3. Calibrate: `Radar -> Radiometric -> Calibrate`

    In Tab2 change saving output to 'Save as complex output' instead of 'output sigma0 band'
![calibrate output](calibrate.png)

4. Deburst: `Radar -> Sentinel-1 TOPS -> S-1 TOPS Deburst`

5. Generate C2 Metrix: `Radar -> Polarimetric -> Polarimetric Matrix Generation`

    In tab2 choose `C2` metrix, and this step should be done after deburst.
![C2 Metrix](c2metrix.png)

6. Multilooking: `Radar -> Multilooking`

7. Terrain Correction: `Radar -> Geometric -> Terrain Correction -> Range-Doppler Terrain Correction`

    > Make sure to deselect option 'Mask out areas without elevation' when dealing with ocean area.

8. Write

## Process with SNAP gpt tool

1. Config environmet variable in system `PATH` 

2. Type command `gpt Apply-Orbit-File -h` in `cmd`

3. Type command `C:\Users\mengy>gpt Apply-Orbit-File -Ssource="D:\DATA\Sentinel\Xuzhou\Unzip\S1A_IW_SLC__1SDV_20160805T101148_20160805T101215_012464_01379E_AF42\S1A_IW_SLC__1SDV_20160805T101148_20160805T101215_012464_01379E_AF42.SAFE" -t "C:\Users\mengy\Documents\DATA\Sentinel\test\S1A_IW_SLC__1SDV_20160805T101148_20160805T101215_012464_01379E_AF42_orb_test.dim"  ` in `cmd`

## Process with SNAP gpt graph tool

1. `Read` -> `$input1`

2. `write` -> `output`
C:\Users\mengy\Documents\DATA\Sentinel\test\S1A_IW_SLC__1SDV_20160805T101148_20160805T101215_012464_01379E_AF42_Orb_Cal_Deb_mrg_ML_TC.tif

## Process with QGIS

1. Clip output file `S1_xuzhou_20160805.tif` with `xuzhou_centre.shp`

    > target CRS: `EPSG:4326 - WGS 84`
    > assign a specified nodata value to output band to `0.00` (*otherwise output a rectangle*)

2. Export output to computer disk

    > save as `S1_xuzhou_20160805_clip.tif`