---
title: Post-PS-SARscape Analysis in ArcGIS Pro
date: 2022-05-17 12:25:43
banner_img: /2022/05/17/Post-PS-SARscape-Analysis-in-ArcGIS-Pro/connect.png
index_img: /2022/05/17/Post-PS-SARscape-Analysis-in-ArcGIS-Pro/r.jpg
tags:
    - SARscape
    - ArcGIS Pro
    - R
    - RStudio
    - PS-InSAR
categories:
    - PS-InSAR
---

This post is about post-SARscape-PSinsar analysis for Xuzhou city in ArcGIS and RStudio. 

# R-ArcGIS bridge

R is an open-source statistical computing language that offers a large suite of data analysis and statistical tools. You can use R to extend the capabilities of ArcGIS and allow for more robust spatial and non-spatial data analysis. In this tutorial, a process will be outlined which will guide you through configuring ArcGIS with the R-ArcGIS bridge to enable you to access ArcGIS datasets in R, install third-party R packages, and execute R tools configured as script tools in an ArcGIS toolbox. By establishing this link, you will be able to develop R programs that interoperate with ArcGIS datasets, and create custom geoprocessing tools that use R functionality within an ArcGIS Pro or ArcGIS Desktop  environment[1].

## Background Information

> ArcGIS offers a multitude of statistical tools for data analysis. These tools are especially useful when working with GIS data, as they can help you extract information that might not be apparent by looking at a map. The tools available in ArcGIS include basic descriptive statistics (e.g., mean, median, standard deviation), and a variety of spatial statistics, many of which can be found in the Spatial Statistics Toolbox. You are encouraged to use these built-in tools where applicable.

> While these resources can be beneficial, certain common non-spatial analysis techniques (e.g., ANOVA, correlation, etc.) are not available from within ArcGIS. If you need to apply such techniques, you may have to draw on other statistical software, and exporting data from ArcGIS to a third-party system to perform a simple test can be time-consuming and sometimes cumbersome. Being able to communicate with other statistical software systems directly from within ArcGIS helps to alleviate this problem. This is made possible by the R-ArcGIS Bridge by connecting ArcGIS to the R statistical computing software.

> R is an open-source statistical computing language that offers a large suite of data analysis and statistical tools. R also has an active and growing user community of data scientists, academics, and software engineers who are constantly implementing new tools and techniques that keep R up to date with the latest research methods. Harnessing these tools can reduce some of the work of performing statistical tests, and is a reason to consider using R with ArcGIS.

> Setting up the R-ArcGIS Bridge to establish the link between ArcGIS and R is a relatively straightforward process. By completing this tutorial, you will have successfully installed the R-ArcGIS Bridge. This initial setup is a requirement for associated tutorial that teach you to work with ArcGIS data in R, and create custom R tools that can be executed from ArcGIS.

## ArcGIS pro

1. Load all SARscape PS results in ArcGIS (`H:\DATA\Sentinel\Xuzhou_1\PS\xuzhou_centre_PS_processing\geocoding`). 

2. `Merge` into one dataset `PS_SARscape_xzc_merge`[2].

3. Symbology



[1]: https://highered-esricanada.github.io/r-arcgis-tutorials/1-Getting-Started.pdf "Tutorial for ArcGIS Pro 2.2 / ArcGIS Desktop 10.6.1 Getting Started with the R-ArcGIS Bridge"
[2]: https://www.esri.com/arcgis-blog/products/arcgis-desktop/mapping/merging-multiple-shapefiles-into-a-single-shapefile/ 