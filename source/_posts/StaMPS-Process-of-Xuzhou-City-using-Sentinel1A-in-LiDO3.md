---
title: StaMPS Process of Xuzhou City using Sentinel1A in LiDO3
date: 2022-05-11 12:04:54
index_img: /2022/05/11/StaMPS-Process-of-Xuzhou-City-using-Sentinel1A-in-LiDO3/Geographical-location-of-Xuzhou-city-Jiangsu-Province-China.png
banner_img: /2022/05/11/StaMPS-Process-of-Xuzhou-City-using-Sentinel1A-in-LiDO3/xuzhou.jpg
tags:
    - InSAR 
    - Land Subsidence
    - Sentinel1-A
    - PS-InSAR
    - LiDO3
    - Xuzhou
    - StaMPS-InSAR
    - snap2stamps
categories:
    - SAR
---

# Intro 

To monitor the land subsidence inside Xuzhou city and to compare the resule of StaMPS-InSAR with SARscape PS process, and to understand the difference of these two methods.

# Xuzhou City

<!-- more -->

Xuzhou, or known as Pengcheng in ancient times, is a major city in and the fourth largest prefecture-level city of Jiangsu province. It is known for its role as a transportation hub in northwestern Jiangsu, as it has expressways and railway links connecting directly to the provinces of Henan and Shandong, the neighboring port city of Lianyungang, as well as the economic hub Shanghai[1].

Xuzhou is "a city of science and education" with a galaxy of talents. There are altogether 31 independent scientific research institutes, 335 R&D institutions and 14 colleges and universities, including China University of Mining & Technology, Xuzhou Medical College, Jiangsu Normal University, Xuzhou Institute of Technology, etc, which jointly provide Xuzhou with solid and strong intellectual support for its economic and social development.

## Location

Xuzhou is located in the northwest of Jiangsu Province, the common boundary of Jiangsu Province, Shandong Province, Henan Province and Anhui Province. It is about 300 kilometers away from Nanjing, Jinan, Zhengzhou and Hefei. Covering a land of 1,125 square kilometers, Xuzhou has a population of 8.63 million.

The prefecture-level city of Xuzhou administers 10 county-level divisions, including five districts, two county-level cities and three counties.

## Climate

By the influence of warm temperature zone semi-moist monsoon climate, Xuzhou has apparently different four seasons with cool summer and warm winter. The yearly average temperature is 14℃ with the yearly average rainfall volume of 866mm and the frost free period in Xuzhou is between 200 and 220 days. The yearly average sunshine period is 2,100 hours to 2,400 hours. The best time for traveling in Xuzhou is in fall.

![Xuzhou](xuzhou1.jpg)

## Economy

Xuzhou is rich in resources and diversified in agricultural and sideline produce. It has a huge reserve of high-quality mineral resources such as coal, iron, limestone, marble stone, etc.

The industrialization management of agriculture has achieved great progress. The guiding role of industrial economy has being continuously intensified. Coal, electronic, fabric, medicine and construction material industries have certain scope and level.

Xuzhou Construction Machinery Group (XCMG), Weiwei Group and Datun Coal and Electricity Corporation are listed the top 520 corporations in China. Tertiary industry has developed boomingly and the role of regional logistics center and tourism center has further intensified. Foreign-oriented economy has made great headway. Many world well-known companies such as Caterpillar, Rockwell and Haier Group settled here.

The construction machinery manufacturer XCMG is the largest company based in Xuzhou. It is the world's 10th largest construction equipment maker measured by 2011 revenues, and the third-largest based in China (after Sany and Zoomlion).

## History and Culture

With a civilization of over 5,000 years, Xuzhou was built 2,600 years ago and is one of the most ancient cities in China. Xuzhou was one of the nine states of the country over 3,000 years ago.

As a city of Han Dynasty Culture, Xuzhou was the hometown of Liu Bang (256-195BC), the first emperor of Western Han Dynasty. The Han Dynasty was divided into Eastern Han and Western Han periods (206BC-189AD) lasting for over 400 years. During which the local kings in Xuzhou left numerous historical heritages, including Han clay figurines, Han stone relief carvings and Han Tombs, which are called "Three Wonders of the Han Dynasty". They are representatives of the Chinese Han culture.

![Xuzhou](xuzhou2.jpg)

# Dataset

134 Sentinel1-A SLC IW arcsending SAR data with path `142` and frame `106` from `2016-08-29` to `2021-11-07` has been acquired from ASF data search vertex[2]. 

![Preview of Sentinel 1A data](dataset.png)

File name: `S1A_IW_SLC__1SDV_20190428T101159_20190428T101227_026989_0309CA_8C7C`

```
Start Time • 04/28/19, 10:11:59Z
Stop Time • 04/28/19, 10:12:27Z
Beam Mode • IW
Path • 142
Frame • 106 
Flight Direction • ASCENDING 
Polarization • VV+VH 
Absolute Orbit • 26989
Data courtesy of ESA
Citation
```

# Pre-process with snap2stamps

## WSL and Xlaunch

In Xlaunch: 
    `One large window` -> `Start no client` -> `Disable access control`

Ubuntu on Windows

```shell
yuchi@DESKTOP-C79LC47:~$ export DISPLAY=$(ip route list default | awk '{print $3}'):0
yuchi@DESKTOP-C79LC47:~$ export LIBGL_ALWAYS_INDIRECT=1
yuchi@DESKTOP-C79LC47:~$ startlxde
** Message: 17:46:13.991: main.vala:102: Session is LXDE
** Message: 17:46:13.991: main.vala:103: DE is LXDE
** Message: 17:46:14.060: main.vala:134: log directory: /home/yuchi/.cache/lxsession/LXDE
** Message: 17:46:14.060: main.vala:135: log path: /home/yuchi/.cache/lxsession/LXDE/run.log
```

## SNAP Desktop

- Select optimal master in SNAP using `Radar / Interferometric / InSAR Stack Overview`

![Select optimal master in SNAP](master_select.png)

- Perform subsetting of whole image using TOPSAR Split via `Radar / Sentinel-1 TOPS / S-1 TOPS Split`. Set the processing parameters
    - subswath IW2
    - polarization  Vv
    - bursts 6-10

![Subsetting of whole image](deburst.png)

- Get LAT/LON MIN/MAX (bounding box) for PSI area of interest. This can be obtained e.g. from ROI polygon in QGIS `Layer Properties | Metadata | Properties | Extent` or ArcGIS. 

Extent of Study area:
    Top: 34.585279
    Bottom: 34.022282
    Left: 116.811255
    Right: 117.713315

## snap2stamps in WSL 

- Edit `project.conf` to set up configuration for your project.

```conf
######### CONFIGURATION FILE ######
###################################
# PROJECT DEFINITION
PROJECTFOLDER=/mnt/i/SAR/Data/Xuzhou_city/
GRAPHSFOLDER=/home/yuchi/software/snap2stamps/graphs
##################################
# PROCESSING PARAMETERS
IW1=IW2
MASTER=/mnt/i/SAR/Data/Xuzhou_city/master/S1A_IW_SLC__1SDV_20190919T101207_20190919T101235_029089_034D30_8446_split.dim
# AOI BBOX DEFINITION
LONMIN=116.811255
LATMIN=34.022282
LONMAX=117.713315
LATMAX=34.585279
##################################
# SNAP GPT 
GPTBIN_PATH=/home/yuchi/snap/bin/gpt
##################################
# COMPUTING RESOURCES TO EMPLOY
CPU=2
CACHE=16G
##################################
```

- Move the master (zip + TOPS - Split Output) to the directory `master` in your PROJECTFOLDER `/mnt/i/SAR/Data/Xuzhou_city/master/`.

- Make sure that all slave images (zip) are stored in the subfolder `slaves` in the PROJECTFOLDER `/mnt/i/SAR/Data/Xuzhou_city/slaves/`.

- Check if all libraries are available for your Python 2 installation (you might need to `pip install pathlib`).

- Run the python scripts of snap2stamp directly in your shell:

```shell
# slave sorting
# (fast) 2-3 seconds
# in WSL
python slaves_prep.py project.conf

# slave splitting and orbit correction
# (this takes some time, approx. 50 seconds per slave) 120 seconds per slave
# in WSL

python splitting_slaves.py project.conf

# master-slave coregistration and interferometric generation
# (this takes some time, approx. 180 seconds per slave) 680+ seconds per slave, around 24 hours to process all
python coreg_ifg_topsar.py project.conf

# ouput data generation in StaMPS compatible format
# (approx. 30 seconds)
# python stamps_export.py project.conf
```

- **DO** Copy `/Xuzhou_city/coreg /graphs /ifg /logs` folder to LiDO use Winscp and run the fellowing scripts in LiDO.

- **DO** Edit the `project.conf` file in `\work\smyumeng\snap2stamps\bin`:

    ```conf
    ######### CONFIGURATION FILE ######
    ###################################
    # PROJECT DEFINITION
    PROJECTFOLDER=/work/smyumeng/Sentinel_PS/xuzhou_city/
    GRAPHSFOLDER=/work/smyumeng/snap2stamps/graphs
    ##################################
    # PROCESSING PARAMETERS
    IW1=IW2
    MASTER=/work/smyumeng/Sentinel_PS/xuzhou_city/master/S1A_IW_SLC__1SDV_20190919T101207_20190919T101235_029089_034D30_8446_split.dim
    ##################################
    # AOI BBOX DEFINITION
    LONMIN=116.811255
    LATMIN=34.022282
    LONMAX=117.713315
    LATMAX=34.585279
    ##################################
    # SNAP GPT 
    GPTBIN_PATH=/home/smyumeng/snap/bin/gpt
    ##################################
    # COMPUTING RESOURCES TO EMPLOY
    CPU=16
    CACHE=160G
    ##################################
    ```

- **DO** cd to `/work/smyumeng/project/snap2stamps_script/` and open in terminal

```shell
sbatch snap2stamps_stamps_output.sh
```

- The data final output structure after performing the last step should contain these four folders: `rslc`, `diff0`, `geo` and `dem`.

- Check for empty interferograms. If any exist, remove files containing the date of the empty file from the folders `rslc` and `diff0`. Otherwise, this will throw warnings related to 0 mean amplitude during the final preparation step use in stamps (i.e. `mt_prep_snap`) and eventually screw up the selection of PS candidates.

- copy `INSAR_master_date` folder from MATLAB workstation

- prepare for StaMPS MATLAB process 
```shell
$ source /work/smyumeng/StaMPS/StaMPS_CONFIG.bash
$ mt_prep_snap 20190919 /work/smyumeng/Sentinel_PS/Xuzhou_city/INSAR_20190919 0.4 3 2 50 200
```

- launch `matlab` to continue with StaMPS PS analysis

## MATLAB

In LiDO, `cd` to shell scripts folder. In this case is `cd /work/smyumeng/project/sentinel_scripts`, edit script `stamps13`, `stamps678` and run shell scripts. 

```
sbatch stamps1_4.sh
sbatch stamps5.sh
sbatch stamps6.sh
sbatch stamps7_8.sh
```

- PS_info

```matlab
>> ps_info
  1  05-Aug-2016   -31 m   53.765 deg
  2  17-Aug-2016    15 m   51.862 deg
  3  29-Aug-2016    24 m   49.975 deg
  4  20-May-2017   -65 m   43.667 deg
  5  13-Jun-2017    -4 m   40.435 deg
  6  25-Jun-2017    30 m   39.724 deg
  7  19-Jul-2017    -4 m   41.734 deg
  8  31-Jul-2017   -51 m   44.778 deg
  9  12-Aug-2017   -49 m   42.578 deg
 10  24-Aug-2017    55 m   38.236 deg
 11  05-Sep-2017    56 m   40.778 deg
 12  17-Sep-2017   -42 m   37.375 deg
 13  11-Oct-2017  -112 m   46.294 deg
 14  23-Oct-2017   -58 m   35.192 deg
 15  04-Nov-2017    18 m   35.254 deg
 16  16-Nov-2017   -13 m   33.478 deg
 17  28-Nov-2017   -83 m   33.531 deg
 18  10-Dec-2017   -47 m   35.339 deg
 19  22-Dec-2017    40 m   35.074 deg
 20  03-Jan-2018    59 m   38.786 deg
 21  15-Jan-2018     4 m   37.448 deg
 22  27-Jan-2018   -32 m   42.990 deg
 23  08-Feb-2018   -89 m   34.891 deg
 24  20-Feb-2018   -76 m   32.894 deg
 25  04-Mar-2018   -59 m   41.880 deg
 26  28-Mar-2018   -10 m   29.675 deg
 27  09-Apr-2018   -32 m   30.275 deg
 28  21-Apr-2018    -9 m   42.681 deg
 29  03-May-2018    45 m   28.915 deg
 30  15-May-2018   -13 m   30.676 deg
 31  27-May-2018   -21 m   30.958 deg
 32  08-Jun-2018   -34 m   32.425 deg
 33  20-Jun-2018   -19 m   32.944 deg
 34  02-Jul-2018    58 m   28.058 deg
 35  14-Jul-2018    60 m   30.617 deg
 36  26-Jul-2018    32 m   48.992 deg
 37  07-Aug-2018     1 m   34.364 deg
 38  19-Aug-2018   -74 m   42.650 deg
 39  31-Aug-2018   -49 m   32.299 deg
 40  12-Sep-2018    14 m   27.427 deg
 41  24-Sep-2018    55 m   26.934 deg
 42  06-Oct-2018    19 m   26.798 deg
 43  18-Oct-2018  -110 m   28.005 deg
 44  11-Nov-2018   -37 m   29.667 deg
 45  23-Nov-2018    34 m   29.855 deg
 46  05-Dec-2018   -19 m   40.999 deg
 47  17-Dec-2018   -11 m   31.237 deg
 48  29-Dec-2018  -117 m   30.164 deg
 49  10-Jan-2019   -37 m   40.075 deg
 50  22-Jan-2019   -52 m   28.011 deg
 51  03-Feb-2019   -12 m   35.595 deg
 52  27-Feb-2019  -115 m   26.293 deg
 53  11-Mar-2019  -111 m   24.771 deg
 54  23-Mar-2019   -92 m   25.106 deg
 55  04-Apr-2019   -59 m   24.716 deg
 56  16-Apr-2019   -10 m   24.913 deg
 57  28-Apr-2019  -122 m   28.848 deg
 58  10-May-2019   -18 m   25.191 deg
 59  22-May-2019   -24 m   27.777 deg
 60  03-Jun-2019    -3 m   27.778 deg
 61  15-Jun-2019    60 m   27.487 deg
 62  27-Jun-2019    -9 m   27.436 deg
 63  09-Jul-2019   -19 m   27.882 deg
 64  21-Jul-2019   -36 m   30.697 deg
 65  02-Aug-2019    59 m   33.582 deg
 66  14-Aug-2019   -42 m   30.115 deg
 67  26-Aug-2019   -78 m   29.975 deg
 68  07-Sep-2019   -98 m   29.634 deg
 69  19-Sep-2019     0 m   27.047 deg
 70  01-Oct-2019   -84 m   28.263 deg
 71  13-Oct-2019    49 m   28.745 deg
 72  25-Oct-2019    43 m   26.720 deg
 73  06-Nov-2019   -88 m   23.890 deg
 74  18-Nov-2019  -130 m   26.777 deg
 75  30-Nov-2019   -53 m   37.977 deg
 76  12-Dec-2019     3 m   27.751 deg
 77  24-Dec-2019     0 m   29.870 deg
 78  05-Jan-2020   -44 m   41.667 deg
 79  17-Jan-2020   -90 m   34.344 deg
 80  29-Jan-2020   -74 m   29.120 deg
 81  10-Feb-2020   -37 m   25.491 deg
 82  22-Feb-2020   -25 m   26.369 deg
 83  05-Mar-2020   -62 m   25.282 deg
 84  17-Mar-2020  -114 m   24.210 deg
 85  29-Mar-2020   -85 m   24.555 deg
 86  10-Apr-2020   -24 m   27.543 deg
 87  22-Apr-2020     2 m   26.744 deg
 88  04-May-2020    -2 m   24.821 deg
 89  16-May-2020   -24 m   26.998 deg
 90  28-May-2020   -35 m   30.625 deg
 91  09-Jun-2020    43 m   28.305 deg
 92  21-Jun-2020    14 m   28.372 deg
 93  03-Jul-2020    75 m   30.817 deg
 94  15-Jul-2020     2 m   31.607 deg
 95  27-Jul-2020   -88 m   33.561 deg
 96  08-Aug-2020   -43 m   34.077 deg
 97  20-Aug-2020    89 m   33.414 deg
 98  01-Sep-2020    -4 m   33.438 deg
 99  13-Sep-2020   -74 m   29.420 deg
100  25-Sep-2020  -115 m   29.306 deg
101  07-Oct-2020  -126 m   27.926 deg
102  19-Oct-2020   -27 m   26.992 deg
103  31-Oct-2020     5 m   27.406 deg
104  12-Nov-2020   -59 m   26.295 deg
105  24-Nov-2020   -26 m   33.760 deg
106  06-Dec-2020   -96 m   27.112 deg
107  18-Dec-2020   -95 m   28.680 deg
108  30-Dec-2020    -6 m   58.614 deg
109  11-Jan-2021    24 m   37.044 deg
110  23-Jan-2021     6 m   32.530 deg
111  04-Feb-2021   -54 m   28.520 deg
112  16-Feb-2021   -65 m   29.469 deg
113  28-Feb-2021   -46 m   41.582 deg
114  12-Mar-2021   -27 m   31.155 deg
115  24-Mar-2021   -44 m   27.754 deg
116  05-Apr-2021   -35 m   30.524 deg
117  17-Apr-2021   -45 m   31.035 deg
118  29-Apr-2021   101 m   33.577 deg
119  11-May-2021     4 m   30.074 deg
120  23-May-2021   -25 m   30.407 deg
121  04-Jun-2021   -37 m   35.760 deg
122  16-Jun-2021   -76 m   34.532 deg
123  28-Jun-2021    23 m   34.516 deg
124  10-Jul-2021    36 m   35.456 deg
125  22-Jul-2021    24 m   34.594 deg
126  03-Aug-2021   -25 m   36.501 deg
127  15-Aug-2021   -15 m   36.154 deg
128  27-Aug-2021   -57 m   44.687 deg
129  08-Sep-2021   -12 m   35.791 deg
130  20-Sep-2021    34 m   40.224 deg
131  02-Oct-2021   -69 m   36.497 deg
132  14-Oct-2021   -92 m   34.842 deg
133  26-Oct-2021  -104 m   34.270 deg
134  07-Nov-2021   -43 m   45.862 deg
Number of stable-phase pixels: 654086
```

- PS_plot

![phase unwrap results](ps_plot_u.png)

```matlab
>> ps_plot('v-do', 'ts');
Deramping computed on the fly. 
**** z = ax + by+ c
654086 ref PS selected
Color Range: -5.53427 to 8.19528 mm/yr
```

- Result

TS plot for default parameter.

![TS plot](ps_result1.png)

Selected point coordinates (lon,lat):116.8939, 34.0225

![Time series plot for #points](ts_result1.png)

Selected point coordinates (lon,lat):116.9868, 34.132

![[Time series plot for #points](ts_result2.png)

## Process 2

Change parameter to below and restart process.


| Parameter           | Default | Description | 
|---------------------|---------|-------------|
| `scla_deramp`       | 'n'     | 'y'         | 
| `unwrap_gold_n_win` | 32      | 8          | 
| `unwrap_grid_size`  | 200     | 50        | 
| `unwrap_time_win`   | 730     | 60        | 
| `scn_time_win`      | 365     | 120        |


Rerun step6 

```
sbatch stamps6.sh
```

## Post-StaMPS

After getting the result of stamps, copy the files to windows through Winscp, furthur analysis is in Matlab.

### Write csv file in Matlab
Make a `csv` file with all data vor visualizations:

```MATLAB
ps_plot('v-do', 'ts');
% after the plot has appeared magically, set radius and location by clicking into the plot
% set radius to 50000 m, and set a point in plot
% Please select a point on the figure to plot time series (TS)
% Selected point coordinates (lon,lat):117.161, 34.1283
```

```matlab
load parms.mat;
ps_plot('v-do', -1);
load ps_plot_v-do.mat;
lon2_str = cellstr(num2str(lon2));
lat2_str = cellstr(num2str(lat2));
lonlat2_str = strcat(lon2_str, lat2_str);

lonlat_str = strcat(cellstr(num2str(lonlat(:,1))), cellstr(num2str(lonlat(:,2))));
ind = ismember(lonlat_str, lonlat2_str);

disp = ph_disp(ind);
disp_ts = ph_mm(ind,:);
export_res = [lon2 lat2 disp disp_ts];

metarow = [ref_centre_lonlat NaN transpose(day)-1];
k = 0;
export_res = [export_res(1:k,:); metarow; export_res(k+1:end,:)];
export_res = table(export_res);
writetable(export_res,'stamps_tsexport.csv')
```
![Figure v-do](ts_plot.png)

### StamPS_visualizer

- open the `StaMPS_Visualizer.Rproj` file with Rstudio

- run `install.packages("renv")` in the R Console

- run `renv::restore()` in the R Console to restore the complete environment (this might take some time)

- go to File --> Open File... and open `ui.R`

- click on Run App in the upper left panel in the upper right corner

#### Resample of stamps results

- Create `roi.kml` in Google Earth 

- Open R.Studio

- Open file `subset_ts_export.R` and edit inputs and outputs

- Run script

## Redo snap2stamps and steps after

Due to some errors in PS_result, we redo all the processes in Lido. 

- Copy folder `I:\SAR\Data\Xuzhou_city\master` and `\slaves` to LiDO `work\smyumeng\Sentinel_PS\xuzhou_city_new`

- Rerun snap2stamps

```bash
######### CONFIGURATION FILE ######
###################################
# PROJECT DEFINITION
PROJECTFOLDER=/work/smyumeng/Sentinel_PS/Xuzhou_city_new/
GRAPHSFOLDER=/work/smyumeng/snap2stamps/graphs
##################################
# PROCESSING PARAMETERS
IW1=IW2
MASTER=/work/smyumeng/Sentinel_PS/Xuzhou_city_new/master/S1A_IW_SLC__1SDV_20190919T101207_20190919T101235_029089_034D30_8446_split.dim
##################################
# AOI BBOX DEFINITION
LONMIN=116.42
LATMIN=33.81
LONMAX=117.61
LATMAX=34.81
##################################
# SNAP GPT 
GPTBIN_PATH=/home/smyumeng/snap/bin/gpt
##################################
# COMPUTING RESOURCES TO EMPLOY
CPU=28
CACHE=256G
##################################
```

```shell
python slaves_prep.py project.conf

python splitting_slaves.py project.conf

python coreg_ifg_topsar.py project.conf

python stamps_export.py project.conf
```


- Rerun StaMPS 

- prepare for StaMPS MATLAB process 
```shell
$ source /work/smyumeng/StaMPS/StaMPS_CONFIG.bash
$ mt_prep_snap 20190919 /work/smyumeng/Sentinel_PS/Xuzhou_city/INSAR_20190919 0.4 3 2 50 200
```

- In LiDO, `cd` to shell scripts folder. In this case is `cd /work/smyumeng/project/sentinel_scripts`, edit script `stamps13`, `stamps678` and run shell scripts. 

```
sbatch stamps1_4.sh
sbatch stamps5.sh
sbatch stamps6.sh
sbatch stamps7_8.sh
```


To be continued!


[1]: http://english.jsjyt.edu.cn/2015-12/08/c_46782.htm "Xuzhou"
[2]: https://search.asf.alaska.edu/#/?zoom=7.701&center=117.637,32.499&polygon=POLYGON((117.0208%2034.0743,117.5322%2034.0743,117.5322%2034.3928,117.0208%2034.3928,117.0208%2034.0743))&start=2014-06-14T22:00:00Z&end=2021-12-11T22:59:59Z&productTypes=SLC&beamModes=IW&subtypes=SA&path=142-&resultsLoaded=true&granule=S1A_IW_SLC__1SDV_20211201T101219_20211201T101247_040814_04D85B_8DCC-SLC&frame=106- "ASF data search vertex"
