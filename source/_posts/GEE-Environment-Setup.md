---
title: GEE_Environment_Setup
date: 2023-02-13 22:54:07
tags:
    - GEE
    - Sentinel
    - Python
categories:
    - GEE
---

# Intro

This post is about enviroment setup of Google Earth Engine.

# GEE

## Python Environment Setup - VS Code

### Error

1. ee.Initialize() - URLError: <urlopen error [WinError 10060]

> URLError: <urlopen error [WinError 10060] A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond>

**Solution:** 安装鉴权验证依赖库[1]：`pip install pyCrypto`


[1]: https://blog.csdn.net/weixin_43360896/article/details/108174759
