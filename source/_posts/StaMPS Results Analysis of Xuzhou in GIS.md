---
title: StaMPS Results Analysis of Xuzhou in GIS
date: 2022-09-16 17:36:42
tags:
    - StaMPS-InSAR
    - Sentinel_1A
    - Xuzhou
    - ArcGIS Pro
    - QGIS
    - Land Subsidence
categories:
    - GIS
---

# Intro 

After export `csv` file in MATLAB, further analysis should be done in GIS program, like ArcGIS or QGIS. This post is about works after 'Post StaMPS Analysis in Xuzhou'.

# ArcGIS Pro

ArcGIS pro is a powerful tool that provides possibilities to work in both ArcGIS online and desktop. After publishing our `stamps_tsexport_d2.csv` to server, it can be edited both within or add the layer in desktop.

- `Add data` to ArcGIS pro from portal.

- Export feature to `StamPS_Analysis/stamps_ts_xuzhou.shp`

*Possible solution*

- Visualization & Exploration of Large Datasets Using Feature Binning[1].

- Strategies to Effectively Display Large Amounts of Data in Web Apps[2].

- Mapping large datasets on the web[3].

## ArcGIS Desktop

1. XY Table to Point

```
Input Table: stamps_tsexport_d2.csv

Output Feature Class: stamps_xzc

X Field: export_res_1

Y Field: export_res_2

Coordinate System: `Current Map` WGS_1984_UTM_Zone_50N
```

2. Lat/Lon to UTM 

3. Symbology

- Apply Filter

- Graduated color

## ArcGIS Online

1. Import `csv` to `stamps_tsexport_xuzhou_city` feature layer.

2. Export data to `shapefile`

- Add a filter

3. Clip `stamps_tsexport_xuzhou_city`dataset with `xuzhou_centre_Dissolve.shp`

4. Symbology

- Apply Filter

- Graduated color

> Error: Maximum sample size reached. Not all records are being used to classify data. View Advanced symbology options to increase maximum sample size.

> Set `Maximum sample size` in `Advanced symbology options` to 1000000.

-

## ArcGIS API for Javascript




# QGIS


[1]: https://www.youtube.com/watch?v=B3KttrQF73g "Visualization & Exploration of Large Datasets Using Feature Binning"
[2]: https://www.esri.com/arcgis-blog/products/data-management/mapping/strategies-to-effectively-display-large-amounts-of-data-in-web-apps/
[3]: https://www.esri.com/arcgis-blog/products/js-api-arcgis/mapping/mapping-large-datasets-on-the-web/ "Mapping large datasets on the web" 
