---
title: Classification_Machine Learning and Deep Learning based LULC in LiDO3
date: 2023-03-16 19:28:06
tags:
    - Python
    - Coding
    - LULC
    - Classification
    - LiDO3
    - Deep Learning
    - Machine Learning
categories:
    - Machine Learning
    - LULC
---

Some error occured during data processing in LiDO3.

**Important:** Be sure to use `python3 xx.py` and `pip3 install --user packages` in processing.

# LiDO3 codes

```shell
$ module add python/3.7.7

$ pip3 install --user tifffile 

$ module add gdal/3.6.1-gcc122

$ python3 RandomForest.py

```

# Errors

## 1. ImportError: No module named tifffile

**Solution:** 

> `python`程序中使用 `import XXX` 时，`python`解析器会在当前目录、已安装和第三方模块中搜索 `xxx`，如果都搜索不到就会报错。使用`sys.path.append()`方法可以临时添加搜索路径，方便更简洁的`import`其他包和模块。这种方法导入的路径会在`python`程序退出后失效。此时，可以通过增加path的方法来解决[1]:

```python
import sys
sys.path.append('./venv/lib/python3.6/site-packages')
sys.path.append('./venv/lib64/python3.6/site-packages')
sys.path.append('/home/smyumeng/.local/lib/python3.6/site-packages')
sys.path.append('/cluster/sfw/gdal/3.6.1-gcc122/lib/python3.7/site-packages')

```

## 2. SyntaxError: Non-ASCII character '\xc2' in file

> YOU WILL LIKELY GET THIS ERROR if you import a PYTHON 3 file into the PYTHON 2 interpreter[2]. (This question should not be closed - '\xc2' is a very particular sort of problem - and very different to that raised by the supposed duplicate question. The answer should be made clear here).

**Solution:**

`Python3 RandomForest.py` instead of `python RandomForest.py`

## 3. 

[1]: https://www.cnblogs.com/kongzhongqijing/articles/13558726.html
[2]: https://stackoverflow.com/questions/18078851/syntaxerror-of-non-ascii-character
