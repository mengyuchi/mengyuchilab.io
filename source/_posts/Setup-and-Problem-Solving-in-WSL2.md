---
title: Setup and Problem-Solving in WSL2
date: 2022-04-14 17:26:29
tags:
    - Linux
    - WSL
    - Ubuntu
categories:
    - Linux
---

Notes about working in WSL! 

<!-- more -->

# How to set up working X11 forwarding on WSL2?

- Installing the desktop (GUI) in WSL

```shell
sudo apt update
sudo apt upgrade
```

> Then by the following command, we can install the light ubuntu desktop

```shell
sudo apt install lxde
```
> In the next step, the [VcXsrv Windows X](https://sourceforge.net/projects/vcxsrv/files/latest/download) Server utility should be installed in windows.
> After installation, we should open VCxsrv software and set the Initial setting.
> Then we should run the following commands in ubuntu terminal[1].

**make sure to check 'disable access control' when configure Xlancher**

Add the following to your ~/.bashrc[2]:

```shell
export DISPLAY=$(ip route list default | awk '{print $3}'):0
export LIBGL_ALWAYS_INDIRECT=1
startlxde 
# main command to run the desktop
# each time that we want to use it we should rerun the last three commands
```
*: If you use VcXSrv you can enable public access for your X server by disabling Access Control on the Extra Settings.

# Install SNAP in WSL 

## Download SNAP and Install


```shell
yuchi@DESKTOP-C79LC47:~/Downloads/snap$ wget https://download.esa.int/step/snap/8.0/installers/esa-snap_sentinel_unix_8_0.sh

yuchi@DESKTOP-C79LC47:~/Downloads/snap$ chmod +x esa-snap_sentinel_unix_8_0.sh
yuchi@DESKTOP-C79LC47:~/Downloads/snap$ ./esa-snap_sentinel_unix_8_0.sh
```
![SNAP install in WSL](SNAP_install.png)

All DONE! Enjoy SNAP in WSL.

**Suddenly all worked out when download snap2stamps in WSL2, but not runing in actural windows.**.

Enjoy processing!




[1]: https://forum.step.esa.int/t/psi-using-snap-stamps-workflow-in-windows-subsystem-for-linux-wsl/23004 "PSI using SNAP-STaMPS workflow in Windows Subsystem for Linux (WSL)"
[2]: https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2 "How to set up working X11 forwarding on WSL2"