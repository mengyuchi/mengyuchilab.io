---
title: SAR image denoising using deep learning
date: 2024-09-04 11:30:33
tags:
    - SAR
    - Denoising
    - Deep Learning
categories:
    - Denoising
---

This post is about denoising of SAR image using sevaral deep learning methods.

# MoNET_pytorch

## Errors

1. RuntimeError: CUDA out of memory. 

Tried to allocate 7.68 GiB (GPU 0; 47.37 GiB total capacity; 38.60 GiB already allocated; 7.07 GiB free; 38.62 GiB reserved in total by PyTorch)

# SAR-CNN 

## Build environment in AutoDL

1. Create new `conda` environment: `conda create --name sarcnn python=3.7`

2. Activate `sarcnn` environment: `source activate sarcnn`

3. Optional: install `mumba`: `conda install conda-forge/label/cf202003::mamba`

4. Install `numpy`: `conda install numpy=1.16.5`

5. Install `tensorflow-gpu`: `conda install tensorflow-gpu=1.13.1`

6. Install `pillow`: `conda install anaconda::pillow`

## Run SAR-CNN scripts

1. `python main.py --real_sar=1 --test_dir='/autodl-fs/SAR-CNN-test/test_real/'`

2. `chmod -R 755 ~/autodl-fs/SAR-CNN-test/`

3. `(sarcnn) root@autodl-container-dc124d9a08-a11e31e5:~/autodl-fs/SAR-CNN-test# python main.py --real_sar=1 --test_dir='~/autodl-fs/SAR-CNN-test/test_real/'`

4. Upload real Sentinel1-A SAR data into folder `/root/autodl-fs/SAR-CNN-test/data/real`

5. Run script `/root/autodl-fs/SAR-CNN-test/main.py`
