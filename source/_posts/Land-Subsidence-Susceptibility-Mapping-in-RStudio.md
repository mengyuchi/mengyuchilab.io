---
title: Land Subsidence Susceptibility Mapping in RStudio
date: 2023-06-08 10:30:33
tags:
    - Land Subsidence Susceptibility Map
    - Land Subsidence
    - ANFIS
    - MATLAB
categories:
    - Land Subsidence
---

Processes of land subsidence susceptibility map using RStudio.

# Setup RStudio

# Jackknife in RStudio


