---
title: Words in InSAR Research
date: 2022-03-23 10:52:20
tags:
    - InSAR
    - Reposititory
categories:
    - Landuage
---

# Words in InSAR Research

This post is about every new words about InSAR during research, and it's references! 

<!-- more --> 

## New Word

![word_brick](brick.jpg "word") 



| Word   | Meaning       | Chinese | Example |
|--------|---------------------------------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [retain](https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1) | <ul><li>to keep or continue to have something</li><li>If a substance retains something, such as heat or water, it continues to hold or contain it.</li></ul>   | 保留，保持，保有 <br><br> 保存；留住；容纳 | SBAS-InSAR techniques can overcome the limitations of DInSAR to some degree by analyzing the pixels that **retain** stable scattering characteristics in a time series to obtain ground deformation information. |
| [preliminary](https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1) | <ul><li>*adj* coming before a more important action or event, especially introducing or preparing for it</li><li>*noun* an event or action that introduces or prepares for something else | 初步的，起始的；预备的 <br></br> 开端活动，初步行动 | Then, the development of land subsidence in BTH was **preliminarily** investigated. |
| [suppress]((https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1)) | <ul><li>to end something by force</li><li>to prevent something from being seen or expressed or from operating</li></ul> | （用武力）镇压，压制，制止<br></br>抑制，忍住，压抑；查禁 | First, the low-pass filtering in the spatial domain is used to **suppress** the noise phase from the residual. |
| [superpose](https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1) | <ul><li> to lay or place on, over, or above something else</li><li> *Geometry* to make (one figure) coincide with another in all parts, by or as if by placing one on top of the other</li></ul> | vt. 放在上面,重叠 | Thirdly, we **superpose** the non-linear phase to the linear phase to acquire the complete deformation phase. |
| [alleviate](https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1) | <ul><li> **VERB** If you alleviate pain, suffering, or an unpleasant condition, you make it less intense or severe.</li><li> | vt. 减轻, 缓解, 缓和 | Since the MSWDP began operation in 2015, the urban water supply has been **alleviated**, and land subsidence has been slowed to some degree. | 
| [fluctuation](https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1) | *NOUN* <ul><li>continual change from one point or condition to another </li><li> wavelike motion; undulation </li><li> *Genetics* a body variation due to environmental factors and not inherited | 变动, <行市> 混乱 <br></br>波动,起伏,涨落  | In Figure 14, the groundwater level shows a drop with a significant **fluctuation** trend, especially in wells h to j. |
| [resultant](https://www.tandfonline.com/doi/full/10.1080/15481603.2016.1227297) | *ADJECTIVE* <ul><li> that results; following as a consequence </li><li> resulting from two or more forces or agents acting together </li></ul><ul><li> *NOUN* </li><li> something that results; result </li><li> *Physics* a force, velocity, etc. with an effect equal to that of two or more such forces, etc. acting together</li></ul> | *形容词*  作为结果而产生的 <br></br> 合成的 <br></br> *物理* 合力 <br></br> *可数名词* <br></br> 结果[of] <br></br> *物理*合力,合成运动 <br></br> *数学*结式 | Doppler separations are below the threshold values, which depend on data availability for the specific application and the expected rate of decorrelation for the given terrain, ensures that the **resultant** network of image pairs contains no isolated clusters |
| [fluvial](https://dspace.library.uu.nl/handle/1874/375843) | *ADJECTIVE* of, relating to, or occurring in a river | 1 河川的 <br></br>因河流的作用而形成的 | The delta is impacted by decreased **fluvial** sediment supply, salinization, coastal erosion and global sea-level rise. |
| [sediment](https://dspace.library.uu.nl/handle/1874/375843) | *VARIABLE NOUN* Sediment is solid material that settles at the bottom of a liquid, especially earth and pieces of rock that have been carried along and then left somewhere by water, ice, or wind. |  沉淀物,渣 <br></br> 『地质』沉积物 | The delta is impacted by decreased fluvial **sediment** supply, salinization, coastal erosion and global sea-level rise. |
| [inundation](https://dspace.library.uu.nl/handle/1874/375843) | <ul><li> *VERB* If you say that you are inundated with things such as letters, demands, or requests, you are emphasizing that you receive so many of them that you cannot deal with them all. </li><li> *VERB* [usually passive] If an area of land is inundated, it becomes covered with water. | 泛滥,淹水,洪水 <br></br> 充满,涌到[of]an ~ of letters ( 雪片般) 涌到的信函  | which increases its vulnerability to flooding, salinization, storm surges, coastal erosion and, ultimately, threatens the delta with permanent **inundation**. |
| [iterative](https://github.com/dbekaert/StaMPS/tree/master/Manual) | <ul><li>repetitious or frequent</li><li>mathematics, logic another word for *recursive*</li><li>grammar another word for frequentative</li></ul> | *adj.* 重复的<br></br>反复的<br></br>迭代的 | This is an **iterative** step that estimates the phase noise value for each candidate pixel in every interferogram. |
| [sparse]() | *ADJECTIVE* Something that is sparse is small in number or amount and spread out over an area. | <<形容词>> <人口等> 稀疏的,稀少的 <br></br> <头发、树丛等> 稀疏的 | Unfortunately, as many deltas around the world are located in data **sparse** regions, high-accuracy elevation data is often not available, or - when existing - not publicly accessible.|



[1]: https://www.mdpi.com/2072-4292/12/3/457?type=check_update&version=1 "Chaofan Zhou, Huili Gong. Land Subsidence Response to Different Land Use Types and Water Resource Utilization in Beijing-Tianjin-Hebei, China"
[2]: https://www.tandfonline.com/doi/full/10.1080/15481603.2016.1227297 "Chaofan Zhou, Huili Gong. Land Subsidence under different land use in the eastern Beijing Plain, China 2005-2013 revealed by InSAR timeseries analysis"
[3]: https://dspace.library.uu.nl/handle/1874/375843 "Minderhoud, Philip Simon Johannes. The sinking mega-delta: Present and future subsidence of the Vietnamese Mekong delta, (2019) Utrecht Studies in Earth Sciences, volume 168 (Dissertation)"
[4]: https://github.com/dbekaert/StaMPS/tree/master/Manual "StaMPS_Manual.pdf"
