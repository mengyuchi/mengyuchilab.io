---
layout: psot
title: Getting into Land Subsidence Mornitoiring using ENVISAT Dataset
date: 2022-04-01 17:48:11
tags:
    - InSAR
    - Land Subsidence
    - ENVISAT
    - StamPS
    - Reposititory
categories: 
    - SAR 
---

# Getting into Land Subsidence Mornitoring using ENVISAT Dataset

This post is about knowladge build of land subsidence mornitoring using ENVISAT[1] dataset.

<!-- more -->

![ENVISAT ASAR](ENVISAT.png)

(a) Illustration of ENVISAT ASAR geometry for the Wide-swath Mode, which consists of five narrow-swath beams, each covering a width between 70 and 100 km. Source: ESA 2002. The width of the wide-swath image is about 400 km. (b) Illustration and main terms of satellite SAR geometry. Source: Robinson 2004. Courtesy: Springer, Praxis Publishing Ltd[2]. 



> @ABraun[3] 

```
Stripmap data is less difficult, because you don't need Split, Deburst...

For using ENVISAT in StaMPS I recommend

Subset (if necessary)
Apply orbit file
coregistration (one master, multiple slaves)
Interferogram generation (including topographic phase removal)
StaMPS export with the outcomes of #3 and #4
```

> So where do I download Envest images?

```
here: https://esar-ds.eo.esa.int/oads/access/collection
```

> Which image mode should I download?

```
IMS because it has phase information.
```

> Can I process different Tracks together?

```
no, InSAR requires data from the same track.
```

# Data Process

## SNAP 

- Apply orbit file 

- Subset for  ENVISAT dataset

    North 

    West 

    South

    East 

> POLYGON ((117.426 34.548, 116.711 34.548, 116.711 33.922, 117.426 33.922, 117.426 34.548))

![subset by SNAP](subset1.png)

> POLYGON ((117.408 34.501, 117.408 33.922, 116.711 33.922, 116.711 34.501, 117.408 34.501))

- Coregistration 

> Find optimal master, others options with default 

- Interferogram formation with topographic phase removal


## StamPS-InSAR 

StaMPS PS analysis is performed within MATLAB, using the main function stamps(), which takes the first and last StaMPS-step as main arguments. To run StaMPS complete with default arguments, use stamps(1,8)[4].






[1]: https://earth.esa.int/web/eoportal/satellite-missions/e/envisat "Basics of EnviSat (Environmental Satellite)"
[2]: https://www.researchgate.net/publication/263040396_Sea_Ice_Monitoring_by_Remote_Sensing "Sea Ice Monitoring by Remote Sensing"
[3]: https://forum.step.esa.int/t/how-to-prepare-ers-envisat-images-stack-for-psi-in-snap5/6346/13 "STEP fprum"
[4]: https://gitlab.com/Rexthor/gis-blog/-/blob/master/StaMPS/2-4_StaMPS-steps.md "StaMPS tutorial by Matthias Schloegl"